#!/bin/bash
set -x

sudo /usr/local/bin/pip3 install --upgrade awscli  #docevents fix in bootcore

# Install JDBC interpreter
sudo chmod 777 /etc/zeppelin/conf/zeppelin-env.sh 
sudo cp /etc/zeppelin/conf/zeppelin-env.sh /etc/zeppelin/conf/zeppelin-env.sh-back
sudo service zeppelin stop

# add a newline char to the file
sudo sed -i -e '$a\' /etc/zeppelin/conf/zeppelin-env.sh
sudo echo '# Start Custom Settings' | sudo tee -a  /etc/zeppelin/conf/zeppelin-env.sh
sudo echo 'export ZEPPELIN_INTERPRETER_DEP_MVNREPO="https://repo1.maven.org/maven2"' | sudo tee -a  /etc/zeppelin/conf/zeppelin-env.sh 
sudo echo 'export ZEPPELIN_MEM="-Xms5g -Xmx5g -XX:MaxMetaspaceSize=5120m"' >> /etc/zeppelin/conf/zeppelin-env.sh
sudo echo 'export ZEPPELIN_INTP_MEM="-Xmx35G -XX:MaxPermSize=5120m"' >> /etc/zeppelin/conf/zeppelin-env.sh
sudo echo 'export JAVA_TOOL_OPTIONS="-Dzeppelin.interpreter.dep.mvnRepo=http://insecure.repo1.maven.org/maven2/"' | sudo tee -a  /etc/zeppelin/conf/zeppelin-env.sh 
sudo /usr/lib/zeppelin/bin/install-interpreter.sh --name jdbc
#sudo aws s3 cp s3://${1}/system/config-files/interpreter.json-emr-5.33.0 /etc/zeppelin/conf.dist/interpreter.json
sudo cp /etc/zeppelin/conf.dist/interpreter.json /etc/zeppelin/conf.dist/interpreter.json_bak
sudo aws s3 cp s3://${1}/system/config-files/interpreter.json /etc/zeppelin/conf.dist/interpreter.json

sudo /usr/local/bin/pip3 install certbot certbot-dns-route53

# Setup SSL
unset PYTHON_INSTALL_LAYOUT # important for some reason
#sudo /usr/bin/pip-2.7 uninstall -y parsedatetime && sudo /usr/bin/pip-2.7  install parsedatetime==2.5
sudo /usr/local/bin/pip3 uninstall -y parsedatetime && sudo /usr/local/bin/pip3  install parsedatetime==2.5
# copy current folder structure from s3 bucket to master node 
aws s3 cp s3://${1}/user/ssl/clientkeys.tar.gz /tmp/clientkeys.tar.gz && sudo tar -C / -zxvf /tmp/clientkeys.tar.gz && sudo rm -f /tmp/clientkeys.tar.gz

# Enabling run schedule from zeppelin-site.xml as per client request.
# Run scheduler option is removed from Zeppelin 0.8 version due to security reasons.
sudo mv /usr/lib/zeppelin/conf/zeppelin-site.xml zeppelin-site.xml.bak
sudo aws s3 cp s3://${1}/system/config-files/zeppelin-site.xml /usr/lib/zeppelin/conf/zeppelin-site.xml

# Copy over zeppelin-web WAR file, modified with TMX styling
# sudo mv /usr/lib/zeppelin/zeppelin-web-0.8.1.war /usr/lib/zeppelin/zeppelin-web-0.8.1.war.bak
sudo mv /usr/lib/zeppelin/zeppelin-web-0.9.0.war /usr/lib/zeppelin/zeppelin-web-0.9.0.war.bak
sudo aws s3 cp s3://${1}/system/zeppelin-web-0.8.2.war /usr/lib/zeppelin/zeppelin-web-0.8.2.war	

# renew certificate if file exists else download new certificates
serverName=`sudo cat /etc/hostname`
currentPrivKey=`sudo ls -lrt /etc/letsencrypt/live/${serverName}/privkey.pem |wc -l`
if [[ "$currentPrivKey" > 0 ]]; then
    echo "Certificates Found!"
    sudo /usr/local/bin/certbot renew
    sudo /usr/local/bin/certbot update_symlinks
else
    sudo /usr/local/bin/certbot certonly -n --agree-tos --email datasupport@tmx.com --dns-route53 -d `sudo cat /etc/hostname`
fi
sudo /opt/tmx/install_ssl.sh `sudo cat /etc/hostname` ${1} && sudo /usr/lib/zeppelin/bin/zeppelin-daemon.sh stop && sudo /usr/lib/zeppelin/bin/zeppelin-daemon.sh start
sudo /opt/tmx/install_ldap.sh `cat /mnt/var/lib/info/job-flow.json | jq -r ".jobFlowId"` ${1}
#(crontab -l 2>/dev/null; echo "0  13  *  *  *  sudo /opt/letsencrypt/letsencrypt-auto --no-bootstrap renew") | crontab -
(crontab -l 2>/dev/null; echo "0  13  *  *  *  sudo /usr/local/bin/certbot renew") | crontab -
(crontab -l 2>/dev/null; echo "0  14  *  *  *  sudo /opt/tmx/install_ssl.sh `sudo cat /etc/hostname` ${1} && sudo /usr/lib/zeppelin/bin/zeppelin-daemon.sh stop && sudo /usr/lib/zeppelin/bin/zeppelin-daemon.sh start") | crontab -
		   
#Update python version in pyspark
sudo /usr/local/bin/pip3 install pyspark
sudo sed -i 's+python34+/usr/bin/python+g' /etc/spark/conf/spark-env.sh

sudo /usr/lib/zeppelin/bin/zeppelin-daemon.sh stop && sudo /usr/lib/zeppelin/bin/zeppelin-daemon.sh start

# Configuration for Tez-ui followed by RM restart
sudo cp /mnt/var/lib/tomcat/webapps/tez-ui/config/configs.env /mnt/var/lib/tomcat/webapps/tez-ui/config/configs.env.orig
sudo sed -i "s+timeline:.*+timeline: \"http://$(dig +short `sudo cat /etc/hostname`):8188\",+g" /mnt/var/lib/tomcat/webapps/tez-ui/config/configs.env

#sudo stop hadoop-yarn-resourcemanager
#sudo start hadoop-yarn-resourcemanager
sudo systemctl stop hadoop-yarn-resourcemanager.service
sudo systemctl start hadoop-yarn-resourcemanager.service

# Configure Presto -- this is done here and not via EMR App config because it only needs to be on master node.
echo "http-server.https.enabled=true" | sudo tee -a /etc/presto/conf/config.properties
echo "http-server.https.keystore.path=/etc/presto/keystore.jks" | sudo tee -a /etc/presto/conf/config.properties
echo "http-server.https.port=8899" | sudo tee -a /etc/presto/conf/config.properties
echo "http-server.authentication.type=PASSWORD" | sudo tee -a /etc/presto/conf/config.properties
echo "http-server.https.keystore.key=keypass" | sudo tee -a /etc/presto/conf/config.properties
echo "internal-communication.authentication.ldap.user=internal" | sudo tee -a /etc/presto/conf/config.properties
echo "internal-communication.authentication.ldap.password=`tail -n -1 /opt/tmx/apiconfig`" | sudo tee -a /etc/presto/conf/config.properties
echo 'password-authenticator.name=ldap' | sudo tee -a /usr/lib/presto/etc/password-authenticator.properties
echo 'ldap.user-bind-pattern=uid=${USER},DC=root' | sudo tee -a /usr/lib/presto/etc/password-authenticator.properties
echo "ldap.url=ldaps://`sudo cat /etc/hostname`:1390" | sudo tee -a /usr/lib/presto/etc/password-authenticator.properties

sudo echo "-Xmx48G" | sudo tee -a /etc/presto/conf/jvm.properties
sudo sed -i '/Xmx/c\-Xmx24G' /etc/presto/conf/jvm.config
sudo sed -i '/Xmn/c\-Xmn512M' /etc/presto/conf/jvm.config

sudo service presto-server stop && sudo service presto-server start
# sudo stop presto-server
# sudo start presto-server

# install SSH keys
aws s3 cp s3://${1}/system/authorized_keys - >> /home/hadoop/.ssh/authorized_keys || true

#setup Monitor script
sudo aws s3 cp s3://${1}/system/CWAgent.sh /opt/tmx/CWAgent.sh
sudo chmod +x /opt/tmx/CWAgent.sh
sudo /opt/tmx/CWAgent.sh
sudo aws cloudwatch put-metric-alarm --alarm-name `sudo cat /etc/hostname` --alarm-description "Alarm if an Masternode is using more memory which marked as Unhealthy" --actions-enabled --alarm-actions arn:aws:sns:us-east-1:865124799329:ClientEMR_monitor --metric-name MemoryUtilization --namespace System/Linux --statistic Average --evaluation-periods 1 --dimensions Name=InstanceId,Value=`curl -sL http://169.254.169.254/latest/meta-data/instance-id` --period 300 --threshold 90 --comparison-operator GreaterThanOrEqualToThreshold --region us-east-1

# load presto autoscaling scripts
cd /home/hadoop
sudo aws s3 cp s3://${1}/system/presto-cloudwatch/setup-presto-cloudwatch.sh .
sudo chmod +x /home/hadoop/setup-presto-cloudwatch.sh

# lines for stop/start datadog-agent
#sudo stop datadog-agent
#sudo start datadog-agent

# Fix issue with Ganglia
sudo aws s3 cp s3://${1}/system/fix_ganglia.sh /home/hadoop/.
sudo chmod +x /home/hadoop/fix_ganglia.sh
sudo /home/hadoop/fix_ganglia.sh